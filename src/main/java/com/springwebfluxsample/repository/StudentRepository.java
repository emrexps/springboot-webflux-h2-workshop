package com.springwebfluxsample.repository;


import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import com.springwebfluxsample.model.Student;

public interface StudentRepository extends ReactiveCrudRepository<Student, Long> {

}
