package com.springwebfluxsample;

import org.springframework.boot.WebApplicationType;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;

@SpringBootApplication
public class SpringBootH2WebFluxApplication {
	public static void main(String[] args) {
		new SpringApplicationBuilder(SpringBootH2WebFluxApplication.class).web(WebApplicationType.REACTIVE).run(args);
	}
}
