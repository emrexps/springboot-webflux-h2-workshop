package com.springwebfluxsample.handler;


import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import com.springwebfluxsample.model.Student;
import com.springwebfluxsample.repository.StudentRepository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.net.URI;

@Service
public class StudentHandler {
    private final StudentRepository studentRepository;

    public StudentHandler(StudentRepository studentRepository) {
        this.studentRepository = studentRepository;
    }

    public Mono<ServerResponse> getStudentById(@PathVariable("id") ServerRequest request) {
        Long studentId = Long.valueOf(request.pathVariable("id"));
        Mono<Student> studentMono = studentRepository.findById(studentId);
        return studentMono.flatMap(student -> ServerResponse.ok().bodyValue(student))
                .switchIfEmpty(ServerResponse.notFound().build());
    }

    public Mono<ServerResponse> getAllStudents(ServerRequest serverRequest) {
        Flux<Student> students = studentRepository.findAll();
        return ServerResponse.ok().body(students, Student.class);
    }

    public Mono<ServerResponse> saveStudent(ServerRequest request) {
        Mono<Student> studentMono = request.bodyToMono(Student.class);
        return studentMono
                .flatMap(student -> studentRepository.save(student))
                .flatMap(savedStudent -> ServerResponse.created(URI.create("/student/" + savedStudent.getId())).build());
    }
}
